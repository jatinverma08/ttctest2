package ttc;



import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;



public class ttcAssign01Test {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
//R1:  A one way trip inside Zone 1
	//Emad travels from Leslie to Don Mills station.
	//Emad is charged $2.50 for the trip. 

	@Test
	public void testOneWayZone1() {
		
		ttcAssign01 b = new ttcAssign01();
		String[] from = {"Leslie"};
		String[] to = {"Don Mills"};
		double fare = b.calculateTotal(from,to);
		assertEquals(2.50, fare,0.0);
		
	}
	//R2: A one way trip inside Zone 2
	//Emad travels from Sheppard to Finch station
	//Emad is charged $3.00 for the trip
	
	@Test
	public void testOneWayZone2() {
		
		ttcAssign01 b = new ttcAssign01();
		String[] from = {"Sheppard"};
		String[] to = {"Finch"};
		double fare = b.calculateTotal(from,to);
		assertEquals(3.00, fare,0.0);
		
	}

	//R3: A trip between zones
	//Emad starts at Don Mills and travels to Finch station
	//Emad is charged $3.00 for the trip
@Test
public void testbetweenZones() {
		
		ttcAssign01 b = new ttcAssign01();
		String[] from = {"Don Mills"};
		String[] to = {"Finch"};
		double fare = b.calculateTotal(from,to);
		assertEquals(3.00, fare,0.0);
		
	}

//R4: More than 1 trip
//Trip 1:  Emad starts at Finch and goes to Sheppard.
//He goes shopping for 30 minutes, then gets on the subway to come to college.
//Trip 2:  Emad starts at Leslie and ends at Don Mills.
//Total trip cost:
//Trip 1 = $3.00
//Trip 2 = $2.50
//Total cost = $5.50

@Test
public void testMoreThan1Trip() {
	
	ttcAssign01 b = new ttcAssign01();
	String[] from = {"Finch", "Leslie"};
	String[] to = {"Sheppard", "Don Mills"};
	double fare = b.calculateTotal(from,to);
	assertEquals(5.50, fare,0.0);
	
}
//R5: Reaching Daily Maximum
@Test
public void testMultiple() {
	
	ttcAssign01 b = new ttcAssign01();
	String[] from = {"Finch", "Sheppard","Finch"};
	String[] to = {"Sheppard", "Finch","Sheppard"};
	double fare = b.calculateTotal(from,to);
	assertEquals(6.00, fare,0.0);
	
}


}
